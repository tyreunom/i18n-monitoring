# i18n monitoring website.
#
# Copyright © 2019, 2020, 2021 Julien Lepiller <julien@lepiller.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

MYDATE=$(shell date +'%Y.%m.%d')

RESULTS=$(shell ls public/data) results.$(MYDATE).json
JSON_RESULTS=$(addprefix public/data/, $(RESULTS))
DAY_IMAGES=$(addsuffix .png, $(addprefix public/images/, $(RESULTS)))
LANG_DATA=$(shell find public/data -mtime -100 -type f | sort) \
    public/data/results.$(MYDATE).json

FDROIDDATA=$(shell ls fdroiddata/metadata/*.yml)
TRANSMON=/root/.config/guix/current/bin/guix environment -L modules --ad-hoc \
  python transmon nss-certs -- transmon

FDROIDDATADIR=fdroiddata
QUIET=@

all: $(DAY_IMAGES) evo
	$(MAKE) public/i18n.css public/i18n.html public/i18n-data.js

fdroid-update:
	@if [ ! -d $(FDROIDDATADIR) ]; then \
	  echo [GIT CLONE] $(FDROIDDATADIR); \
	  git clone https://gitlab.com/fdroid/fdroiddata.git $(FDROIDDATADIR); \
	fi
	@echo [GIT PULL] $(FDROIDDATADIR)
	$(QUIET)cd $(FDROIDDATADIR); git pull

cache/i18n-data-languages.js: $(shell ls public/images/evo-*)
	@if [ ! -d cache ]; then mkdir cache; fi
	@echo [GEN] $@
	$(QUIET)echo "surveyed_languages=[" > $@
	$(QUIET)for f in public/images/evo*; do \
	  echo "'$$(basename $$f | cut -c5- | rev | cut -c5- | rev)'," >> $@; \
	done
	$(QUIET)echo "];" >> $@

cache/i18n-data-dates.js: $(shell ls public/images/results*)
	@if [ ! -d cache ]; then mkdir cache; fi
	@echo [GEN] $@
	$(QUIET)echo "survey_dates=[" > $@
	$(QUIET)for f in public/images/results*; do \
	  echo "'$$(basename $$f | cut -c9- | rev | cut -c10- | rev)'," >> $@; \
	done
	$(QUIET)echo "];" >> $@

public/i18n-data.js: cache/i18n-data-languages.js cache/i18n-data-dates.js
	@if [ ! -d public ]; then mkdir public; fi
	@echo [GEN] $@
	$(QUIET)cat $^ > $@

public/images/results.%.png: public/data/results.% one-day.scm
	@if [ ! -d public/images ]; then mkdir -p public/images; fi
	@echo [ONE-DAY] $@
	$(QUIET)/root/.config/guix/current/bin/guix environment --ad-hoc guile guile-json \
	  gnuplot -- guile -L modules one-day.scm $<

public/i18n.html: static/i18n.html
	@if [ ! -d public ]; then mkdir public; fi
	cp $< $@

public/i18n.css: static/i18n.css
	@if [ ! -d public ]; then mkdir public; fi
	cp $< $@

evo: $(LANG_DATA) one-lang.scm
	@if [ ! -d public/images ]; then mkdir -p public/images; fi
	@echo [EXEC] one-lang.scm
	$(QUIET)/root/.config/guix/current/bin/guix environment --ad-hoc guile guile-json \
	  gnuplot -- guile -L modules one-lang.scm $(LANG_DATA)

public/data/results.$(MYDATE).json: fdroid.manifest
	@if [ ! -d public/data ]; then mkdir -p public/data; fi
	@echo [TRANSMON] $@
	$(QUIET)$(TRANSMON) $< 2>&1 > $@ | tee transmon.log | grep Project

fdroid.manifest: $(FDROIDDATA)
	@echo [FDROIDDATA] $@
	$(QUIET)/root/.config/guix/current/bin/guix environment --ad-hoc \
	  guile guile-json -- guile generate-fdroid.scm $(FDROIDDATADIR)/metadata > $@
