Internationalization Monitoring
===============================

This repository contains scripts and programs used to generate the website at
[i18n.lepiller.eu](https://i18n.lepiller.eu/i18n.html).  Every day, a cron task
updates repositories (currently only fdroid), analyses them to find a list of
free software projects, and analyses these projects to find their translation
status.  Then, the website is generated with graphs and explanations.

Scripts and Usage
-----------------

In order to run the website and analysis, the following script is needed inside
a cron task:

```bash
cd /path/to/this/repository
make fdroid-update
make
```

This creates a website inside the `public` directory.

The Makefile is the main entry point to the analysis.  The `modules` directory
contains important scheme modules used by some of the scripts.  `data-analysis.scm`
contains common procedures for every analysis, while `guix-packages.scm` contains
[guix](https://guix.gnu.org) package definitions.  The `static` directory contains
static website elements: a css and an html file.

Data Provenance
--------------

The set of free software analysed by this repository comes from the sources
below:

### Fdroidata

We analyze the content of the [fdroiddata](https://gitlab.com/fdroid/fdroiddata.git)
repository to find a list of free software android applications.  The
`generate-fdroid.scm` script is used to generate a json file that can be
passed to transmon.

Currently, the script will analyze every file and generate a json object for
every file that contains at least this information: repository url, home page,
name and type of repository.  Since transmon doesn't yet support other types
of repositories, we filter repository types and keep only git repositories
(the vast majority of applications use a git repository).

Data Collection
---------------

Everyday, we collect data from the generated json files: it's passed to transmon
which updates its cache or fetches new repositories.  It then analyses each
application and returns another json file, named `public/data/results.<date>.json`.

This file is publicly available for download.  See
[transmon](https://framagit.org/tyreunom/transmon)'s repository for more details
on how it works.

Data Analysis
-------------

The last step is data analysis.  There are two scripts run to analyse the data
and generate graphs:

### one-day.scm

This script is responsible for generating a summary of a day's data.  It generates
two graphs in `public/images` that represent the percentage of translated
applications in every language.  It also sorts languages by a weighted average
of the number of apps in each translation status category.


### one-lang.scm

This script is responsible for generating a summary of a language status evolution.
It generates multiple graphs in `public/images` that each represent the evolution
of a language over a long period of time.

Website Generation
------------------

The website itself is not very complicated.  The only generated part is a small
script that contains two variables that correspond to the availability of some
graphs and data, `i18n-data.js`.
