;;; i18n monitoring website.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (json) (ice-9 match) (ice-9 rdelim) (srfi srfi-1))

(define fdroid-datadir (cadr (command-line)))
(define fdroid-yml-files
  (let ((dir (opendir fdroid-datadir)))
    (let loop ((result '()) (next (readdir dir)))
      (if (eof-object? next)
        result
        (loop
          (if (and
                (> (string-length next) 4)
                (equal? (substring next (- (string-length next) 4)) ".yml"))
            (cons (string-append fdroid-datadir "/" next) result)
            result)
          (readdir dir))))))
(define fdroid-files (sort fdroid-yml-files string<=?))

(define (generate-data data)
  `(("name" . ,(assoc-ref data "name"))
    ("variants" .
      ,(list->array 1
        `((("fetcher" . "git")
           ("uri" . ,(assoc-ref data "url"))
           ("tags" . ,(list->array 1 '("master")))
           ("name" . "git-master")))))
    ("tags" . ,(list->array 1 '("android" "fdroid")))
    ("home-page" . ,(assoc-ref data "home"))))

(define (uniq-url lst)
  (define (urls lst)
    (map (lambda (data) (assoc-ref data "url")) lst))
  (fold
    (lambda (elem res)
      (if (member (assoc-ref elem "url") (urls res))
        res
        (cons elem res)))
    '()
    lst))

(define (getstring line from)
  (string-trim-both (substring line from)))

(define (parse-txt file)
  (call-with-input-file file
    (lambda (port)
      (let loop ((result '()))
        (let ((s (read-line port)))
          (cond
            ((eof-object? s) result)
            ((eq? (string-contains s "RepoType:") 0)
             (let ((type (getstring s 9)))
               (if (not (equal? type "git"))
                 #f
                 (loop (cons `("type" . "git") result)))))
            ((eq? (string-contains s "Repo Type:") 0)
             (let ((type (getstring s 10)))
               (if (not (equal? type "git"))
                 #f
                 (loop (cons `("type" . "git") result)))))
            ((eq? (string-contains s "AutoName:") 0)
             (loop (cons `("name" . ,(getstring s 9)) result)))
            ((eq? (string-contains s "Auto Name:") 0)
             (loop (cons `("name" . ,(getstring s 10)) result)))
            ((eq? (string-contains s "WebSite:") 0)
             (let ((site (getstring s 8)))
               (if (equal? site "")
                 (loop result)
                 (loop (cons* `("home" . ,site) `("website" . #t) result)))))
            ((eq? (string-contains s "Web Site:") 0)
             (let ((site (getstring s 9)))
               (if (equal? site "")
                 (loop result)
                 (loop (cons* `("home" . ,site) `("website" . #t) result)))))
            ((eq? (string-contains s "SourceCode:") 0)
             (if (assoc-ref result "website")
               (loop result)
               (loop (cons `("home" . ,(getstring s 11)) result))))
            ((eq? (string-contains s "Source Code:") 0)
             (if (assoc-ref result "website")
               (loop result)
               (loop (cons `("home" . ,(getstring s 12)) result))))
            ((eq? (string-contains s "Repo:") 0)
             (loop (cons `("url" . ,(getstring s 5)) result)))
            (else (loop result))))))))

(define (parse-yml file)
  ;; It's actually very similar :)
  (parse-txt file))

(display
  (scm->json-string
    (list->array 1
      (map 
        generate-data
        (uniq-url
          (filter (lambda (a)
                    (and (assoc-ref a "url") (assoc-ref a "type")
                         (assoc-ref a "home") (assoc-ref a "name")))
                  (map parse-txt fdroid-files)))))))
(newline)
