;;; i18n monitoring website.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (data-analysis)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (json)
  #:export (uniq
            project-data
            categorize
            count-cat
            get-langs
            generate-lang-data))

(define (uniq lst)
  "Return only uniq data from lst"
  (let loop ((result '()) (lst lst))
    (if (null? lst)
        result
        (loop (if (member (car lst) result) result (cons (car lst) result)) (cdr lst)))))

(define (project-data lang stats)
  "Return stats only for one language"
  (map (lambda (project) (assoc-ref project lang)) stats))

(define (categorize n)
  "Return the category of a fraction"
  (if n
      (if (eq? n 1)
          2
          (if (> n (/ 90 100))
              3
              (if (> n (/ 70 100))
                  4
                  (if (> n (/ 30 100))
                      5
                      (if (eq? n 0) 1 6)))))
      1))

(define (count-cat cat-stats n)
  "Return the count of projects that are at least in the category @var{n}"
  (exact->inexact
    (/ (length (filter (lambda (cat) (and cat (>= cat n))) cat-stats))
       (length cat-stats))))

(define (any l)
  "Return #t if any value in list @var{l} is true"
  (fold (lambda (a b) (or a b)) #f l))

(define (get-langs stats)
  "Return every language found in stats, ignoring weird names"
  (let ((langs (uniq (apply append (map (lambda (project) (map car project)) stats)))))
    (filter
      (lambda (l)
        (and (not (string-index l char-numeric?))
             (not (string-contains l "keyshidden"))
             (not (string-contains l "keysexposed"))
             (not (any (map (lambda (s) (string-contains l s))
                            '("normal" "xlarge" "small" "large"
                              "hdpi"))))))
      langs)))

(define* (generate-lang-data stats #:key (count-cat count-cat))
  (let* ((langs (get-langs stats)))
    (map (lambda (lang)
           (let* ((lang-data (project-data lang stats))
                  (lang-count
                    (map
                      (lambda (s)
                        (if s
                            (let ((num (array-ref s 0))
                                  (den (array-ref s 1)))
                              (if (eq? den 0) #f (/ num den)))
                            #f))
                      lang-data))
                  (lang-cat (map categorize lang-count)))
             (list lang (count-cat lang-cat 1)
                   (count-cat lang-cat 2)
                   (count-cat lang-cat 3)
                   (count-cat lang-cat 4)
                   (count-cat lang-cat 5)
                   (count-cat lang-cat 6))))
         langs)))
