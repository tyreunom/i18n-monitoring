;;; i18n monitoring website.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system python)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages version-control))

(define-public my-python-android-stringslib
  (package
    (inherit python-android-stringslib)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://framagit.org/tyreunom/python-android-strings-lib")
                     (commit "3e05f3d5e9834cdc38499deacf82e952d5f798e7")))
              (file-name "android-stringlib-source")
              (sha256
               (base32
                "1hpgh48vmn2g5zl5rbzz2z2w7h2b74m6pjln6j5jiq6zbwkxm1m3"))))))

(define-public transmon
  (let ((commit "b0d7aa0a9d47dfe993db64eed61e6058ddd9b780")
        (revision "0"))
    (package
      (name "transmon")
      (version (git-version "0.1.1" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                       (url "https://framagit.org/tyreunom/transmon")
                       (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0krvl6jfainxnmyc7mhxjd61ziidx94l35bv9yf8nw8xza63c4i3"))))
      (build-system python-build-system)
      (arguments
       `(#:tests? #f; no tests
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'fix-libgit2
             (lambda _
               (substitute* "transmon/fetcher.py"
                 (("pygit2.remote.RemoteCallbacks") "pygit2.RemoteCallbacks")))))))
      (propagated-inputs
       `(("python-android-stringslib" ,my-python-android-stringslib)
         ("python-polib" ,python-polib)
         ("python-pygit2" ,python-pygit2)
         ("python-translation-finder" ,python-translation-finder)))
      (home-page "https://framagit.org/tyreunom/transmon")
      (synopsis "")
      (description "")
      (license license:agpl3+))))
