;;; i18n monitoring website.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (data-analysis) (ice-9 match) (json))

(define data-file (cadr (command-line)))
(define full-output-file (string-append "public/images/full-" (basename data-file) ".png"))
(define truncated-output-file (string-append "public/images/" (basename data-file) ".png"))

(define (generate-graph column-count data-file output-file)
  (let ((plot-file (tmpnam)))
    (with-output-to-file plot-file
      (lambda _
        (format #t "set style data histograms~%")
        (format #t "set style fill solid~%")
        (format #t "set xtics rotate~%")
        (format #t "set terminal png size ~a,600~%" (+ 100 (* column-count 14)))
        (format #t "set key at graph 0.05, 0.9~%")
        (format #t "set output '~a'~%" output-file)
        (format #t "plot '~a' using 3:xtic(1) with boxes title 'done (100%)' linecolor rgb '#88ee88',"
                data-file)
        (format #t " '' using 4 with boxes title 'almost done (> 90%)' linecolor rgb '#aaff99',")
        (format #t " '' using 5 with boxes title 'good progress (> 70%)' linecolor rgb '#eedd99',")
        (format #t " '' using 6 with boxes title 'in progress (> 30%)' linecolor rgb '#eecc88',")
        (format #t " '' using 7 with boxes title 'started (> 0%)' linecolor rgb '#ffaa99'~%")))
    (system* "gnuplot" plot-file)
    (delete-file plot-file)))

(define (points r)
  (match r
    ((lang _ c1 c2 c3 c4 c5)
     (+ (- c1 c2) (* 0.9 (- c2 c3)) (* 0.7 (- c3 c4)) (* 0.3 (- c4 c5)) (* 0.1 c5)))))

(define (generate-data-file formated-data-file lang-data)
  (with-output-to-file formated-data-file
    (lambda _
      (format #t "#lang any 100 90 70 30 0~%")
      (for-each
        (lambda (lang)
          (match lang
            ((lang c1 c2 c3 c4 c5 c6)
             (format #t "~a ~a ~a ~a ~a ~a ~a~%" lang c1 c2 c3 c4 c5 c6))))
        (sort lang-data (lambda (r1 r2)
                          (> (points r1) (points r2))))))))

;; Create a file for one day, every lang in X
(catch #t
  (lambda ()
    (let* ((tmp-full (tmpnam))
           (tmp-truncated (tmpnam))
           (data (with-input-from-file data-file json->scm))
           (states (map (lambda (d) (array->list (assoc-ref d "state")))
                        (array->list data)))
           ;; We can have more than one translation state per project, so we
           ;; flatten them here:
           (states (apply append states))
           (stats (map (lambda (s) (assoc-ref s "state")) states))
           (stats-truncated (filter (lambda (s) (and s (not (null? s)))) stats))
           (full-lang (generate-lang-data stats))
           (truncated-lang (generate-lang-data stats-truncated)))
      (generate-data-file tmp-full full-lang)
      (generate-data-file tmp-truncated truncated-lang)
      (generate-graph (length full-lang) tmp-full full-output-file)
      (generate-graph (length truncated-lang) tmp-truncated truncated-output-file)
      (delete-file tmp-full)
      (delete-file tmp-truncated)))
  (lambda _
    (format #t "Unable to produce graphs from ~a. Skipping.~%" data-file)))
