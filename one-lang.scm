;;; i18n monitoring website.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (data-analysis) (ice-9 match) (json))

(define data-files (cdr (command-line)))

(define (count-cat cat-stats n)
  (length (filter (lambda (cat) (and cat (>= cat n))) cat-stats)))

(define (generate-graph data-file output-file)
  (let ((plot-file (tmpnam)))
    (with-output-to-file plot-file
      (lambda _
        (format #t "set style data histograms~%")
        (format #t "set style fill solid~%")
        (format #t "set xtics rotate~%")
        (format #t "set terminal png size 800,600~%")
        (format #t "set key outside~%")
        (format #t "set xtics auto~%")
        (format #t "set output '~a'~%" output-file)
        (format #t "plot '~a' using 2:xtic(int($0)%3 == 0 ? stringcolumn(1) : '') with boxes title 'total analyzed' linecolor rgb '#aaaaaa',"
                data-file)
        (format #t " '' using 3 with boxes title 'done (100%)' linecolor rgb '#88ee88',")
        (format #t " '' using 4 with boxes title 'almost done (> 90%)' linecolor rgb '#aaff99',")
        (format #t " '' using 5 with boxes title 'good progress (> 70%)' linecolor rgb '#eedd99',")
        (format #t " '' using 6 with boxes title 'in progress (> 30%)' linecolor rgb '#eecc88',")
        (format #t " '' using 7 with boxes title 'started (> 0%)' linecolor rgb '#ffaa99'~%")))
    (system* "gnuplot" plot-file)
    (delete-file plot-file)))

(define (generate-data-file formated-data-file lang-data)
  (with-output-to-file formated-data-file
    (lambda _
      (format #t "#lang any 100 90 70 30 0~%")
      (for-each
        (lambda (lang)
          (match lang
            ((lang c1 c2 c3 c4 c5 c6)
             (format #t "~a ~a ~a ~a ~a ~a ~a~%" lang c1 c2 c3 c4 c5 c6))
            ((lang . #f)
             (format #t "~a 0 0 0 0 0 0~%" lang))))
        lang-data))))

(define (read-data-file file)
  (catch #t
    (lambda ()
      (let* ((data (with-input-from-file file json->scm))
             (states (map (lambda (d) (array->list (assoc-ref d "state")))
                          (array->list data)))
             (states (apply append states))
             (stats (map (lambda (s) (assoc-ref s "state")) states)))
        (filter (lambda (s) (and s (not (null? s)))) stats)))
    ;; any error? the file is broken
    (lambda _
      '())))

(define (name->date f)
  (substring (basename f) 8 18))


(format #t "Reading data files...~%")
;; Create a file for one language, every day in X
(let* ((data (map read-data-file data-files))
       (langs (uniq (apply append (map get-langs data))))
       (lang-data (map (lambda (d) (generate-lang-data d #:count-cat count-cat))
                       data))
       (tot (length langs))
       (n 1))
  (format #t "Generating plots...~%")
  (for-each
    (lambda (lang)
      (format #t "[~a / ~a] ~a      \r" n tot lang)
      (force-output)
      (set! n (+ n 1))
      (let* ((output-file (string-append "public/images/evo-" lang ".png"))
             (my-lang-data (map (lambda (filename data)
                                  (cons (name->date filename) (assoc-ref data lang)))
                                data-files lang-data))
             (tmpfile (tmpnam)))
        (generate-data-file tmpfile my-lang-data)
        (generate-graph tmpfile output-file)
        (delete-file tmpfile)))
    langs))
